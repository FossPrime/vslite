# General
- Add hotkey system
- Add project system
- Add auto install of [G4C](https://gitlab.com/vblip/g4c) (git support [#4](https://github.com/kat-tax/vslite/issues/4))
- Add context menu for various actions
- Add dock layout saving for projects
- Add message system to manipulate app for integrations
- Add first integration: [Figma -> React Native](https://github.com/kat-tax/figma-to-react-native)
- Finish `Open Folder...` action

# Code Editor
- Add status bar (line/col number, etc.)
- Add search/replace/refactor in all files
- Add double click tab bar for new Untitled file
- Add support for multiple Untitled files (`Untitled-1`, `Untitled-2`, etc.)
- Add code previewing (italicized filename, double click tab or file in tree to fully open, swap previews)
- Add user and project config support (JSON & visual via [Uniforms](https://uniforms.tools))
- Improve file saving (autosave to temp only for restoring, turn X to dot when unsaved)
- Improve file editor instances (allow multiple tabs of same path)
- Improve auto typings (some libs fail to import types correctly)

# File Tree
- Fix subfolders not updating on filesystem changes
- Add file icons (parse VSCode icon theme format)
- Add handling of renaming files
- Add handling of moving files

# Meta
- Add deploy w/ CF Pages button
- Create custom logo
- Finish this roadmap

# VSLite

> More than Monaco, less than VSCode

### Try it out

https://vslite.dev

### How to run locally

```sh
pnpm i       # Install dependencies
pnpm dev     # Run local dev server
pnpm build   # Build production bundle
```

### Powered by...

- [WebContainers](https://webcontainers.io) _(the shell)_
- [Monaco React](https://monaco-react.surenatoyan.com) _(the editor)_
- [Xterm.js](https://xtermjs.org) _(the terminal)_
- [Dockview](https://dockview.dev) _(the docking ui)_
- [React Complex Tree](https://rct.lukasbach.com) _(the file tree)_

### Community

- [Discord](https://discord.gg/ty2CstRYZ6)
- [Roadmap](./ROADMAP.md)
